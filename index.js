let studentList = []

function addStudent(student) {
	studentList.push(student);
	console.log(student + " was added to the student's list.");
}

function countStudents() {
	console.log("There are total of " + studentList.length + " student/s enrolled.");
}

function printStudents() {
	studentList.sort();
	studentList.forEach(function(list){
		console.log(list);
	});
}

function findStudent(student) {
	let newlist = [];
	for (let i = 0; i < studentList.length; i++) {
		if(studentList[i] == student) {
			newlist.push(studentList[i]);
		}
	}
	if (newlist.length == 1) {
		console.log(newlist[0] + " is an enrollee");
	}
	if (newlist.length > 1) {
		console.log(newlist.join(',') + " are enrollees");
	}
	if (newlist.length == 0) {
		console.log("No student found with the name " + student);
	}
}

function removeStudent(student) {
	for (let i = 0; i < studentList.length; i++) {
		if (studentList[i] == student) {
			delete studentList[i];
			console.log(student + " was removed from the student's list.");
			break;
		}
	}
}